﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HideNSeek
{
    public partial class Form1 : Form
    {
        int Moves;

        Location currentLocation;

        RoomWithDoor livingRoom;
        Room diningRoom;
        RoomWithDoor kitchen;
        Room stairs;
        RoomWithHidingPlace hallway;
        RoomWithHidingPlace bigBedroom;
        RoomWithHidingPlace smallBedroom;
        RoomWithHidingPlace bathroom;

        OutsideWithDoor frontyard;
        OutsideWithDoor backyard;
        OutsideWithHidingPlace garden;
        OutsideWithHidingPlace street;

        Opponent opponent;

        public Form1()
        {
            InitializeComponent();
            createObjects();
            opponent = new Opponent(frontyard);
            ResetGame(false);
        }

        private void createObjects()
        {
            livingRoom = new RoomWithDoor("Living room", "ancient carpet", "in wardrobe", "oak door with a brass handle");
            diningRoom = new Room("Dining room", "crystal chandelier");
            kitchen = new RoomWithDoor("Kitchen", "stainless steel cutlery", "in cupboard", "sliding door");
            stairs = new Room("Stairs", "wooden handrail");
            hallway = new RoomWithHidingPlace("Hallway", "picture with a dog", "in wardrobe");
            bigBedroom = new RoomWithHidingPlace("Big bedroom", "big bed", "under bed");
            smallBedroom = new RoomWithHidingPlace("Small bedroom", "small bed", "under bed");
            bathroom = new RoomWithHidingPlace("Bathroom", "sink and toilet", "in showe");

            frontyard = new OutsideWithDoor("Front yard", false, "oak door with a brass handle");
            backyard = new OutsideWithDoor("Backyard", true, "sliding door");
            garden = new OutsideWithHidingPlace("Garden", false, "in shed");
            street = new OutsideWithHidingPlace("Street", false, "in garage");

            livingRoom.Exits = new Location[] { diningRoom, stairs };
            diningRoom.Exits = new Location[] { livingRoom, kitchen };
            kitchen.Exits = new Location[] { diningRoom };
            stairs.Exits = new Location[] { livingRoom, hallway };
            hallway.Exits = new Location[] { stairs, bigBedroom, smallBedroom, bathroom };
            bigBedroom.Exits = new Location[] { hallway };
            smallBedroom.Exits = new Location[] { hallway };
            bathroom.Exits = new Location[] { hallway };

            frontyard.Exits = new Location[] { backyard, garden, street };
            backyard.Exits = new Location[] { frontyard, garden, street };
            garden.Exits = new Location[] { frontyard, backyard };
            street.Exits = new Location[] { frontyard, backyard };

            livingRoom.DoorLocation = frontyard;
            kitchen.DoorLocation = backyard;
            frontyard.DoorLocation = livingRoom;
            backyard.DoorLocation = kitchen;
        }

        private void MoveToANewLocation (Location newLocation)
        {
            Moves++;
            currentLocation = newLocation;
            RedrawForm();
        }

        private void RedrawForm()
        {
            exitsComboBox.Items.Clear();
            foreach (var locations in currentLocation.Exits)
                exitsComboBox.Items.Add(locations.Name);
            exitsComboBox.SelectedIndex = 0;

            descriptionTextBox.Text = currentLocation.Description + "\r\nMove number: " + Moves + "\r\n";

            if (currentLocation is IHasExteriorDoor)
                goThroughTheDoorButton.Visible = true;
            else
                goThroughTheDoorButton.Visible = false;

            if (currentLocation is IHidingPlace)
            {
                IHidingPlace hidingPlace = currentLocation as IHidingPlace;
                checkButton.Text = "Check " + hidingPlace.HidingPlace;
                checkButton.Visible = true;
            }
            else
                checkButton.Visible = false;
        }

        private void ResetGame(bool displayMessage)
        {
            if (displayMessage)
            {
                MessageBox.Show("You found me in " + Moves + " moves!", "Congratulations");
                IHidingPlace foundLocation = currentLocation as IHidingPlace;
                descriptionTextBox.Text = "You found an opponent in " + Moves + " moves.\r\nHe was hidding " + foundLocation.HidingPlace + ".\r\n";
            }

            Moves = 0;
            hideButton.Visible = true;
            goHereButton.Visible = false;
            checkButton.Visible = false;
            goThroughTheDoorButton.Visible = false;
            exitsComboBox.Visible = false;
        }

        private void goHereButton_Click(object sender, EventArgs e)
        {
            MoveToANewLocation(currentLocation.Exits[exitsComboBox.SelectedIndex]);
        }

        private void goThroughTheDoorButton_Click(object sender, EventArgs e)
        {
            if (currentLocation is IHasExteriorDoor)
            {
                IHasExteriorDoor hasDoor = currentLocation as IHasExteriorDoor;
                MoveToANewLocation(hasDoor.DoorLocation);
            }
            else
                MessageBox.Show("This room don't have doors! You couldn't see this button!");
        }

        private void checkButton_Click(object sender, EventArgs e)
        {
            Moves++;

            if (opponent.Check(currentLocation))
                ResetGame(true);
            else
                RedrawForm();
        }

        private void hideButton_Click(object sender, EventArgs e)
        {
            descriptionTextBox.Clear();

            for (int i = 1; i <= 10; i++)
            {
                opponent.Move();
                descriptionTextBox.Text += i + "...\r\n";
                Application.DoEvents();
                System.Threading.Thread.Sleep(200);
            }

            descriptionTextBox.Text = "Ready or not - here I come!";
            Application.DoEvents();
            System.Threading.Thread.Sleep(1000);

            goHereButton.Visible = true;
            exitsComboBox.Visible = true;
            MoveToANewLocation(livingRoom);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            imageForm myForm = new imageForm();
            myForm.Show();
        }
    }
}
