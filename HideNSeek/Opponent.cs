﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HideNSeek
{
    class Opponent
    {
        public Opponent(Location location)
        {
            myLocation = location;
        }

        private Location myLocation;
        private static Random myRandom = new Random();

        public void Move()
        {
            bool hidden = false;

            do
            {
                if (myLocation is IHasExteriorDoor)
                    if (myRandom.Next(2) == 1)
                    {
                        IHasExteriorDoor hasDoor = myLocation as IHasExteriorDoor;
                        myLocation = hasDoor.DoorLocation;
                    }

                myLocation = myLocation.Exits[myRandom.Next(myLocation.Exits.Length)];

                if (myLocation is IHidingPlace)
                    hidden = true;
            } while (!hidden);
        }

        public bool Check(Location location)
        {
            if (myLocation == location)
                return true;
            return false;
        }
    }
}
