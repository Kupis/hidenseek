﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HideNSeek
{
    class OutsideWithHidingPlace : Outside, IHidingPlace
    {
        public OutsideWithHidingPlace(string name, bool hot, string hidingPlace)
            : base(name, hot)
        {
            HidingPlace = hidingPlace;
        }

        public string HidingPlace { get; private set; }

        public override string Description
        {
            get
            {
                return base.Description + "Someone may be hiding " + HidingPlace + ".\r\n";
            }
        }
    }
}
