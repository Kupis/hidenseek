# HideNSeek
.NET game about seeking computer. Game use upgraded version of model from "Explore the house" project.
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman
## Purpose
The purpose of this application is to learn more about interfaces and inherit.
## Author
* **Patryk Kupis** - [linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/) [gitlab](https://gitlab.com/Kupis)
## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details